# Pelcro iOS SDK

Our SDK helps you to have easier access to the Pelcro platform. Written for Swift 4.2, for now.

## Installation

We recommend using [CocoaPods][642d6fa5] to integrate the Pelcro SDK with your project.

[642d6fa5]: https://cocoapods.org/ "CocoaPods"

Add a `Podfile` (if you don't have one already), by running the following command: `pod init`.

### Step 1

Add the Pelcro pod:

```ruby
pod 'Pelcro'
```

### Step 2

Run `pod install`

### Troubleshooting

In case Cocoapods doesn't find the Pelcro specs or it finds older specs, try updating with `pod update`. After updating, run `pod install`.

### Usage

Import the SDK in your code:

```swift
import Pelcro
```

Initialize the Pelcro with your parameters:

```swift
Pelcro.shared.siteID = "Your site ID"
Pelcro.shared.accountID = "Your account ID"
Pelcro.shared.appSharedSecret = "Your Apple app shared secret" 
Pelcro.shared.isSandbox = false // false - for Production, isSandbox by default true - for Development/Staging (in app purchases)
Pelcro.shared.isStaging = false // false - for Production, isStaging by default true - for Development/Staging (endpoints base URL)
```
**Pelcro.shared.isStaging should be configured before initialization of any manager.**

*Link* "How to get app shared secret" - https://www.appypie.com/faqs/how-can-i-get-shared-secret-key-for-in-app-purchase

#### Site Manager

To *get* a site please use the following API:

```swift
let siteManager = PelcroSiteManager()
siteManager.getSite(with: "Your site ID") { (result: PelcroResult) in
    print(result.data ?? "No data returned")
    print(result.error ?? "No error returned")
}

// Then you can read Site from the same PelcroSiteManager instance, but keep in mind that getSite calls asynchronously 
let site = siteManager.site
```

#### User Manager

To *register* a user please use the following API:

```swift
let userManager = PelcroUserManager()
userManager.register(with: "email@email.com", password: "Your user's password") { (result: PelcroResult) in
    print(result.data ?? "No data returned")
    print(result.error ?? "No error returned")
}

// Then you can read User from the same PelcroUserManager instance, but keep in mind that register calls asynchronously 
let user = userManager.user
```

To *login* a user please use the following API:

```swift
let userManager = PelcroUserManager()
userManager.login(with: "email2@email.com", password: "Your user's password") { (result: PelcroResult) in
    print(result.data ?? "No data returned")
    print(result.error ?? "No error returned")
}

// Then you can read User from the same PelcroUserManager instance, but keep in mind that login calls asynchronously 
let user = userManager.user
```

To *refresh* a user please use the following API:

```swift
let userManager = PelcroUserManager()
userManager.refresh(with: "email2@email.com", password: "Your user's password") { (result: PelcroResult) in
    print(result.data ?? "No data returned")
    print(result.error ?? "No error returned")
}

// Then you can read User from the same PelcroUserManager instance, but keep in mind that refresh calls asynchronously 
let user = userManager.user
```

To *logout* a user please use the following API:

```swift
let userManager = PelcroUserManager()
userManager.logout()

// Then you can check auth token, it should be nil 
let authToken = Pelcro.shared.authToken
```

#### Subscription Manager

To use *PelcroSubscriptionManager* functionality you should have already set up purchases (subscriptions) in App Store Connect
Please use follow links to tutorials:
https://help.apple.com/app-store-connect/#/devb57be10e7
https://help.apple.com/app-store-connect/#/devae49fb316
https://help.apple.com/app-store-connect/#/dev7e89e149d
https://help.apple.com/app-store-connect/#/dev75708c031
https://help.apple.com/app-store-connect/#/dev06f89ce98
https://www.raywenderlich.com/5456-in-app-purchase-tutorial-getting-started

*You should configure Subscriptions in App Store Connect with the same parameters (price, duration, etc) as you have for Plan in Pelcro.
When you have got productID you should enter it for appropriate Plan in Pelcro.*

To *create* a subscription please use the following API:

```swift
let subscriptionManager = PelcroSubscriptionManager(productsIdentifiers: ["<YOUR APPLE PRODUCT ID>"])
subscriptionManager.createSubscription(with: "<YOUR PLAN ID>", appleProductID: "<YOUR APPLE PRODUCT ID>", couponCode: "<YOUE COUPON CODE IF EXIST>", completion: { (result: PelcroSubscriptionResult) in
    print(result.data ?? "No data returned")
    print(result.error ?? "No error returned")
 })
```

To *cancel* a subscription please use the following API *(It cancels subscription only on Pelcro side)*:

```swift
let subscriptionManager = PelcroSubscriptionManager(productsIdentifiers: ["<YOUR APPLE PRODUCT ID>"])
subscriptionManager.cancelSubscription(with: "<YOUR SUBSCRIPTION ID>", completion: { (result: PelcroSubscriptionResult) in
    print(result.data ?? "No data returned")
    print(result.error ?? "No error returned")
 })
```

To *reactivate* a subscription please use the following API *(It reactivates subscription only on Pelcro side)*:

```swift
let subscriptionManager = PelcroSubscriptionManager(productsIdentifiers: ["<YOUR APPLE PRODUCT ID>"])
subscriptionManager.reactivateSubscription(with: "<YOUR SUBSCRIPTION ID>", completion: { (result: PelcroSubscriptionResult) in
    print(result.data ?? "No data returned")
    print(result.error ?? "No error returned")
 })
```

To *check if user subscribed to site* please use the following API *(It checks we have at least one active subscription on Pelcro side and checks existance of invalid subscriptions from App Store, if yes - cancel them on Pelcro side)*:

```swift
let subscriptionManager = PelcroSubscriptionManager(productsIdentifiers: ["<YOUR APPLE PRODUCT ID>"])
subscriptionManager.isSubscribedToSite(user: "<YOUR USER DATA>", completion: { (result: Bool) in
    print(result)
})
```

### License

The Pelcro iOS SDK is open source and available under the MIT license. See the LICENSE file for more info.

