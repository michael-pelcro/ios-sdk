//
//  AuthRequestDataSourceFactory.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

enum AuthRequestDataSourceCreationError: Error {
    case invalidParams
}

struct AuthRequestDataSourceFactory: RequestDataSourceFactory {
    typealias RequestType = AuthRequestType
    typealias BodyInfoType = AuthParamsType
    
    private let baseURLProvider: AnyDataProvider<String>
    private let bodyInfoConverter: AnyDataConverter<AuthParamsType, Data?>
    
    init<BaseURLProvider: DataProvider,
        BodyInfoConverter: DataConverter>
        (baseURLProvider: BaseURLProvider,
         bodyInfoConverter: BodyInfoConverter)
        where BaseURLProvider.DataType == String,
        BodyInfoConverter.DataToConvertType == AuthParamsType,
        BodyInfoConverter.ConvertedType == Data? {
            self.baseURLProvider = baseURLProvider.erased
            self.bodyInfoConverter = bodyInfoConverter.erased
    }
    
    func requestDataSource(for requestType: AuthRequestType,
                           bodyInfo: AuthParamsType) throws -> URLRequestDataSource {
        let urlString = urlStringFor(requestType: requestType)
        let httpMethod = HTTPMethod.post
        let jsonHeaderValue = "application/json"
        let authRequestHeaderValues: [HeaderField: String] = [.accept: jsonHeaderValue,
                                                              .contentType: jsonHeaderValue]
        let headerValues: [HeaderField: String] = authRequestHeaderValues
        let httpBody = bodyInfoConverter.converted(data: bodyInfo)
        switch requestType {
        case .register:
            guard case .register = bodyInfo else {
                throw AuthRequestDataSourceCreationError.invalidParams
            }
        case .login:
            guard case .login = bodyInfo else {
                throw AuthRequestDataSourceCreationError.invalidParams
            }
        case .refresh:
            guard case .refresh = bodyInfo else {
                throw AuthRequestDataSourceCreationError.invalidParams
            }
        }
        return AuthURLRequestDataSource(urlString: urlString,
                                        httpMethod: httpMethod,
                                        headerValues: headerValues,
                                        httpBody: httpBody)
    }
    
    private func urlStringFor(requestType: AuthRequestType) -> String {
        var urlSufix: String
        switch requestType {
        case .register: urlSufix = "auth/register"
        case .login: urlSufix = "auth/login"
        case .refresh: urlSufix = "customer/refresh"
        }
        return "\(baseURLProvider.data)/\(urlSufix)"
    }
}

fileprivate struct AuthURLRequestDataSource: URLRequestDataSource {
    var urlString: String
    var httpMethod: HTTPMethod
    var headerValues: [HeaderField : String]
    var httpBody: Data?
}
