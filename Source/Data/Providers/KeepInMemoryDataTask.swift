//
//  KeepInMemoryDataTask.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

class KeepInMemoryDataTask: AsyncTask {
    typealias ResultType = String
    
    private var dataValue: String?
    private let dataKey: String
    private let parentDataKey: String
    private let urlRequest: URLRequest
    private let task: AnyURLRequestAsyncTask<[String: Any]>?
    
    init<Task: URLRequestAsyncTask>(parentDataKey: String,
                                    dataKey: String,
                                    urlRequest: URLRequest,
                                    task: Task) where Task.ResultType == [String: Any] {
        self.parentDataKey = parentDataKey
        self.dataKey = dataKey
        self.task = task.erased
        self.urlRequest = urlRequest
    }
    
    func runWith(completion: AsyncTaskCompletion?) {
        if let dataValue = dataValue {
            completion?(dataValue, nil)
            return
        }
        task?.run(urlRequest: urlRequest) { result, error in
            let parentDict = result?[self.parentDataKey] as? [String: Any]
            let dataValue = parentDict?[self.dataKey] as? String
            self.dataValue = dataValue
            completion?(dataValue, error)
        }
    }
}
