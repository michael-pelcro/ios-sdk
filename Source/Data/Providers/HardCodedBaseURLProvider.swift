//
//  HardCodedBaseURLProvider.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct HardCodedBaseURLProvider: DataProvider {
    typealias DataType = String
    
    var data: String {
        return Pelcro.shared.isStaging ? "https://staging.pelcro.com/api/v1/sdk" : "https://www.pelcro.com/api/v1/sdk"
    }
}
