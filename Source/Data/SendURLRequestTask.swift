//
//  SendURLRequestTask.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct SendURLRequestTask: URLRequestAsyncTask {
    typealias ResultType = Data
    
    private let urlSession: URLSession
    
    init(urlSession: URLSession) {
        self.urlSession = urlSession
    }
    
    func run(urlRequest: URLRequest, completion: ((Data?, Error?) -> Void)?) {
        let urlRequestTask = urlSession.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) in
            completion?(data, error)
        }
        urlRequestTask.resume()
    }
}
