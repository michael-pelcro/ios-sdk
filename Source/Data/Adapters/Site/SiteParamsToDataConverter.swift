//
//  SiteParamsToDataConverter.swift
//  Pelcro
//
//  Created by Denys Trush on 6/17/19.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct SiteParamsToDataConverter: DataConverter {
    
    typealias DataToConvertType = SiteParamsType
    typealias ConvertedType = Data?
    typealias IntermediaryType = [String: Any]?
    
    private let toDictionaryConverter: AnyDataConverter<DataToConvertType, IntermediaryType>
    private let toDataConverter: AnyDataConverter<IntermediaryType, ConvertedType>
    
    init<ParamsConverter: DataConverter,
        DictionaryConverter: DataConverter>
        (toDictionaryConverter: ParamsConverter,
         toDataConverter: DictionaryConverter)
        where ParamsConverter.DataToConvertType == SiteParamsToDataConverter.DataToConvertType, ParamsConverter.ConvertedType == IntermediaryType,
        DictionaryConverter.DataToConvertType == ParamsConverter.ConvertedType,
        DictionaryConverter.ConvertedType == SiteParamsToDataConverter.ConvertedType {
            self.toDictionaryConverter = toDictionaryConverter.erased
            self.toDataConverter = toDataConverter.erased
    }
    
    func converted(data: SiteParamsType) -> Data? {
        let dictionary = toDictionaryConverter.converted(data: data)
        return toDataConverter.converted(data: dictionary)
    }
}
