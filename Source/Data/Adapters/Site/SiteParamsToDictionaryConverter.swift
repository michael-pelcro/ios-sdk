//
//  SiteParamsToDictionaryConverter.swift
//  Pelcro
//
//  Created by Denys Trush on 6/17/19.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct SiteParamsToDictionaryConverter: DataConverter {
    typealias DataToConvertType = SiteParamsType
    typealias ConvertedType = [String: Any]?
    
    private let siteIDParamName = "site_id"
    
    func converted(data: SiteParamsType) -> [String : Any]? {
        switch data {
        case .siteID(let siteID):
            var dictionary = [String: Any]()
            dictionary[siteIDParamName] = siteID
            return dictionary
        }
    }
}
