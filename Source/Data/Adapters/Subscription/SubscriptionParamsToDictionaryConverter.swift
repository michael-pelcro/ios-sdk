//
//  File.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

struct SubscriptionParamsToDictionaryConverter: DataConverter {
    typealias DataToConvertType = SubscriptionParamsType
    typealias ConvertedType = [String: Any]?
    
    private let siteIDParamName = "site_id"
    private let accountIDParamName = "account_id"
    private let subscriptionIDParamName = "subscription_id"
    private let planParamName = "plan_id"
    private let couponCodeParamName = "coupon_code"
    private let authTokenParamName = "auth_token"
    private let appleIAPReceiptTokenParamName = "apple_iap_receipt"
    private let languageParamName = "language"
    
    func converted(data: SubscriptionParamsType) -> [String : Any]? {
        switch data {
        case .create(let params):
            var dictionary = [String: Any]()
            dictionary[siteIDParamName] = params.baseParams.siteID
            dictionary[accountIDParamName] = params.baseParams.accountID
            dictionary[authTokenParamName] = params.baseParams.authToken
            dictionary[planParamName] = params.planID
            dictionary[couponCodeParamName] = params.couponCode
            dictionary[appleIAPReceiptTokenParamName] = params.appleIAPReceipt
            dictionary[languageParamName] = params.baseParams.language
            return dictionary
        case .cancel(let params):
            var dictionary = [String: Any]()
            dictionary[siteIDParamName] = params.baseParams.siteID
            dictionary[accountIDParamName] = params.baseParams.accountID
            dictionary[authTokenParamName] = params.baseParams.authToken
            dictionary[subscriptionIDParamName] = params.subscriptionID
            dictionary[languageParamName] = params.baseParams.language
            return dictionary
        case .reactivate(let params):
            var dictionary = [String: Any]()
            dictionary[siteIDParamName] = params.baseParams.siteID
            dictionary[accountIDParamName] = params.baseParams.accountID
            dictionary[authTokenParamName] = params.baseParams.authToken
            dictionary[subscriptionIDParamName] = params.subscriptionID
            dictionary[languageParamName] = params.baseParams.language
            return dictionary
        }
    }
}
