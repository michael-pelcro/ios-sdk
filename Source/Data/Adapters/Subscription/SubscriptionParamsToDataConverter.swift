//
//  SubscriptionParamsToDataConverter.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

struct SubscriptionParamsToDataConverter: DataConverter {
    
    typealias DataToConvertType = SubscriptionParamsType
    typealias ConvertedType = Data?
    typealias IntermediaryType = [String: Any]?
    
    private let toDictionaryConverter: AnyDataConverter<DataToConvertType, IntermediaryType>
    private let toDataConverter: AnyDataConverter<IntermediaryType, ConvertedType>
    
    init<ParamsConverter: DataConverter,
        DictionaryConverter: DataConverter>
        (toDictionaryConverter: ParamsConverter,
         toDataConverter: DictionaryConverter)
        where ParamsConverter.DataToConvertType == SubscriptionParamsToDataConverter.DataToConvertType, ParamsConverter.ConvertedType == IntermediaryType,
        DictionaryConverter.DataToConvertType == ParamsConverter.ConvertedType,
        DictionaryConverter.ConvertedType == SubscriptionParamsToDataConverter.ConvertedType {
            self.toDictionaryConverter = toDictionaryConverter.erased
            self.toDataConverter = toDataConverter.erased
    }
    
    func converted(data: SubscriptionParamsType) -> Data? {
        let dictionary = toDictionaryConverter.converted(data: data)
        return toDataConverter.converted(data: dictionary)
    }
}
