//
//  DictionaryToDataConverter.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct DictionaryToDataConverter: DataConverter {
    typealias DataToConvertType = [String: Any]?
    typealias ConvertedType = Data?
    
    func converted(data: [String : Any]?) -> Data? {
        guard let data = data else {
            return nil
        }
        return try! JSONSerialization.data(withJSONObject: data as Any, options: .prettyPrinted)
    }
}
