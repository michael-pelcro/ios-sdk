//
//  SendURLRequestAdapterTask.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct SendURLRequestAdapterTask: URLRequestAsyncTask {
    typealias ResultType = [String: Any]
    
    private let urlRequestTask: AnyURLRequestAsyncTask<Data>
    init<Task: URLRequestAsyncTask>(urlRequestTask: Task) where Task.ResultType == Data {
        self.urlRequestTask =  urlRequestTask.erased
    }
    
    func run(urlRequest: URLRequest, completion: (([String : Any]?, Error?) -> Void)?) {
        urlRequestTask.run(urlRequest: urlRequest) { data, error in
            guard let data = data else {
                completion?(nil, error)
                return
            }
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [String: Any]
                var error: NSError?
                if let errorObjectInJson = jsonObject?["error"] as? [String: Any],
                    let status = errorObjectInJson["status"] as? Int,
                    let message = errorObjectInJson["message"] as? String {
                    error = NSError(domain: "com.pelcro", code: status, userInfo: [NSLocalizedDescriptionKey: message])
                }
                completion?(jsonObject, error)
            }
            catch {
                completion?(nil, error)
            }
        }
    }
}
