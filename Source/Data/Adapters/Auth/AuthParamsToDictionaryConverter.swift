//
//  AuthParamsToDictionaryConverter.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct AuthParamsToDictionaryConverter: DataConverter {
    typealias DataToConvertType = AuthParamsType
    typealias ConvertedType = [String: Any]?
    
    private let siteIDParamName = "site_id"
    private let accountIDParamName = "account_id"
    private let emailParamName = "email"
    private let passwordParamName = "password"
    private let languageParamName = "language"
    private let authTokenParamName = "auth_token"
    
    func converted(data: AuthParamsType) -> [String : Any]? {
        switch data {
        case .login(let params):
            var dictionary = [String: Any]()
            dictionary[siteIDParamName] = params.siteID
            dictionary[emailParamName] = params.email
            dictionary[passwordParamName] = params.password
            return dictionary
        case .register(let params):
            var dictionary = self.converted(data: .login(params.authParams))!
            dictionary[languageParamName] = params.language
            dictionary[accountIDParamName] = params.accountID
            return dictionary
        case .refresh(let params):
            var dictionary = [String: Any]()
            dictionary[siteIDParamName] = params.siteID
            dictionary[accountIDParamName] = params.accountID
            dictionary[authTokenParamName] = params.authToken
            return dictionary
        }
    }
}
