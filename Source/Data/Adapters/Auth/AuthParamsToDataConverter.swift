//
//  AuthParamsToDataConverter.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct AuthParamsToDataConverter: DataConverter {
    
    typealias DataToConvertType = AuthParamsType
    typealias ConvertedType = Data?
    typealias IntermediaryType = [String: Any]?
    
    private let toDictionaryConverter: AnyDataConverter<DataToConvertType, IntermediaryType>
    private let toDataConverter: AnyDataConverter<IntermediaryType, ConvertedType>
    
    init<ParamsConverter: DataConverter,
        DictionaryConverter: DataConverter>
        (toDictionaryConverter: ParamsConverter,
         toDataConverter: DictionaryConverter)
        where ParamsConverter.DataToConvertType == AuthParamsToDataConverter.DataToConvertType, ParamsConverter.ConvertedType == IntermediaryType,
        DictionaryConverter.DataToConvertType == ParamsConverter.ConvertedType,
        DictionaryConverter.ConvertedType == AuthParamsToDataConverter.ConvertedType {
            self.toDictionaryConverter = toDictionaryConverter.erased
            self.toDataConverter = toDataConverter.erased
    }
    
    func converted(data: AuthParamsType) -> Data? {
        let dictionary = toDictionaryConverter.converted(data: data)
        return toDataConverter.converted(data: dictionary)
    }
}
