//
//  SiteRequestDataSourceFactory.swift
//  Pelcro
//
//  Created by Denys Trush on 6/14/19.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

enum SiteRequestDataSourceCreationError: Error {
    case invalidParams
}

struct SiteRequestDataSourceFactory: RequestDataSourceFactory {
    typealias RequestType = SiteRequestType
    typealias BodyInfoType = SiteParamsType
    
    private let baseURLProvider: AnyDataProvider<String>
    private let bodyInfoConverter: AnyDataConverter<SiteParamsType, Data?>
    private let siteIDParamName = "site_id"
    
    init<BaseURLProvider: DataProvider,
        BodyInfoConverter: DataConverter>
        (baseURLProvider: BaseURLProvider,
         bodyInfoConverter: BodyInfoConverter)
        where BaseURLProvider.DataType == String,
        BodyInfoConverter.DataToConvertType == SiteParamsType,
        BodyInfoConverter.ConvertedType == Data? {
            self.baseURLProvider = baseURLProvider.erased
            self.bodyInfoConverter = bodyInfoConverter.erased
    }
    
    func requestDataSource(for requestType: SiteRequestType,
                           bodyInfo: SiteParamsType) throws -> URLRequestDataSource {
        var urlString = urlStringFor(requestType: requestType)
        let httpMethod = HTTPMethod.get
        let headerValues: [HeaderField: String] = [:]
        switch requestType {
        case .getSiteInfo:
            guard case .siteID(let siteID) = bodyInfo else {
                throw AuthRequestDataSourceCreationError.invalidParams
            }
            if let existingSiteID = siteID {
                urlString += "?\(siteIDParamName)=\(existingSiteID)"
            }
        }
        return SiteURLRequestDataSource(urlString: urlString,
                                        httpMethod: httpMethod,
                                        headerValues: headerValues,
                                        httpBody: nil)
    }
    
    private func urlStringFor(requestType: SiteRequestType) -> String {
        var urlSufix: String
        switch requestType {
        case .getSiteInfo: urlSufix = "site"
        }
        return "\(baseURLProvider.data)/\(urlSufix)"
    }
}

fileprivate struct SiteURLRequestDataSource: URLRequestDataSource {
    var urlString: String
    var httpMethod: HTTPMethod
    var headerValues: [HeaderField : String]
    var httpBody: Data?
}
