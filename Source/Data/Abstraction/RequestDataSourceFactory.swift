//
//  RequestDataSourceFactory.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

protocol RequestDataSourceFactory {
    associatedtype RequestType
    associatedtype BodyInfoType
    func requestDataSource(for requestType: RequestType,
                           bodyInfo: BodyInfoType) throws -> URLRequestDataSource
}

extension RequestDataSourceFactory {
    var erased: AnyRequestDataSourceFactory<RequestType, BodyInfoType> {
        return AnyRequestDataSourceFactory(factory: self)
    }
}
