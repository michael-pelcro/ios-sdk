//
//  RequestFactory.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

protocol RequestFactory {
    func requestWith(dataSource: URLRequestDataSource) throws -> URLRequest
}
