//
//  URLRequestDataSource.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

protocol URLRequestDataSource {
    var urlString: String { get }
    var httpMethod: HTTPMethod { get }
    var headerValues: [HeaderField: String] { get }
    var httpBody: Data? { get }
}
