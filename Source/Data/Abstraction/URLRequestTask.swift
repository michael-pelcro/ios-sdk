//
//  URLRequestTask.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

protocol URLRequestAsyncTask {
    associatedtype ResultType
    typealias AsyncTaskCompletion = (ResultType?, Error?) -> Void
    func run(urlRequest: URLRequest, completion: AsyncTaskCompletion?)
}

extension URLRequestAsyncTask {
    var erased: AnyURLRequestAsyncTask<ResultType> {
        return AnyURLRequestAsyncTask(task: self)
    }
}
