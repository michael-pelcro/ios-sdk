//
//  URLRequestFactory.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

enum URLRequestCreationError: Error {
    case invalidURL
}

struct URLRequestFactory: RequestFactory {
    func requestWith(dataSource: URLRequestDataSource) throws -> URLRequest {
        guard let url = URL(string: dataSource.urlString) else {
            throw URLRequestCreationError.invalidURL
        }
        var urlRequest = URLRequest(url: url)
        switch dataSource.httpMethod {
        case .get: urlRequest.httpMethod = "GET"
        case .post: urlRequest.httpMethod = "POST"
        }
        if let acceptHeaderValue = dataSource.headerValues[.accept] {
            urlRequest.addValue(acceptHeaderValue, forHTTPHeaderField: "Accept")
        }
        if let contentTypeHeaderValue = dataSource.headerValues[.contentType] {
            urlRequest.addValue(contentTypeHeaderValue, forHTTPHeaderField: "Content-Type")
        }
        urlRequest.httpBody = dataSource.httpBody
        return urlRequest
    }
}
