//
//  AnyURLRequestAsyncTask.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct AnyURLRequestAsyncTask<R>: URLRequestAsyncTask {
    typealias ResultType = R
    
    typealias CompletionType = ((R?, Error?) -> Void)
    
    private let _run: (URLRequest, CompletionType?) -> Void
    
    init<Task: URLRequestAsyncTask>(task: Task) where Task.ResultType == R {
        _run = task.run
    }
    
    func run(urlRequest: URLRequest, completion: CompletionType?) {
        _run(urlRequest, completion)
    }
}
