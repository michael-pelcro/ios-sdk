//
//  AnyRequestDataSourceFactory.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct AnyRequestDataSourceFactory<R, B>: RequestDataSourceFactory {
    typealias RequestType = R
    typealias BodyInfoType = B
    
    private let _requestDataSource: ((R, B) throws -> URLRequestDataSource)
    
    init<DataSourceFactory: RequestDataSourceFactory>
        (factory: DataSourceFactory)
        where DataSourceFactory.RequestType == R,
        DataSourceFactory.BodyInfoType == B {
            _requestDataSource = factory.requestDataSource
    }
    
    func requestDataSource(for requestType: R,
                           bodyInfo: B) throws -> URLRequestDataSource {
        return try _requestDataSource(requestType, bodyInfo)
    }
}
