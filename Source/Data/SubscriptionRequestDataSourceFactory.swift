//
//  SubscriptionRequestDataSourceFactory.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

enum SubscriptionRequestDataSourceCreationError: Error {
    case invalidParams
}

struct SubscriptionRequestDataSourceFactory: RequestDataSourceFactory {
    typealias RequestType = SubscriptionRequestType
    typealias BodyInfoType = SubscriptionParamsType
    
    private let baseURLProvider: AnyDataProvider<String>
    private let bodyInfoConverter: AnyDataConverter<SubscriptionParamsType, Data?>
    
    init<BaseURLProvider: DataProvider,
        BodyInfoConverter: DataConverter>
        (baseURLProvider: BaseURLProvider,
         bodyInfoConverter: BodyInfoConverter)
        where BaseURLProvider.DataType == String,
        BodyInfoConverter.DataToConvertType == SubscriptionParamsType,
        BodyInfoConverter.ConvertedType == Data? {
            self.baseURLProvider = baseURLProvider.erased
            self.bodyInfoConverter = bodyInfoConverter.erased
    }
    
    func requestDataSource(for requestType: SubscriptionRequestType,
                           bodyInfo: SubscriptionParamsType) throws -> URLRequestDataSource {
        let urlString = urlStringFor(requestType: requestType)
        let httpMethod = HTTPMethod.post
        let jsonHeaderValue = "application/json"
        let authRequestHeaderValues: [HeaderField: String] = [.accept: jsonHeaderValue,
                                                              .contentType: jsonHeaderValue]
        let headerValues: [HeaderField: String] = authRequestHeaderValues
        let httpBody = bodyInfoConverter.converted(data: bodyInfo)
        switch requestType {
        case .create:
            guard case .create = bodyInfo else {
                throw SubscriptionRequestDataSourceCreationError.invalidParams
            }
        case .cancel:
            guard case .cancel = bodyInfo else {
                throw SubscriptionRequestDataSourceCreationError.invalidParams
            }
        case .reactivate:
            guard case .reactivate = bodyInfo else {
                throw SubscriptionRequestDataSourceCreationError.invalidParams
            }
        }
        return AuthURLRequestDataSource(urlString: urlString,
                                        httpMethod: httpMethod,
                                        headerValues: headerValues,
                                        httpBody: httpBody)
    }
    
    private func urlStringFor(requestType: SubscriptionRequestType) -> String {
        var urlSufix: String
        switch requestType {
        case .create: urlSufix = "subscription"
        case .cancel: urlSufix = "subscription/cancel"
        case .reactivate: urlSufix = "subscription/reactivate"
        }
        return "\(baseURLProvider.data)/\(urlSufix)"
    }
}

fileprivate struct AuthURLRequestDataSource: URLRequestDataSource {
    var urlString: String
    var httpMethod: HTTPMethod
    var headerValues: [HeaderField : String]
    var httpBody: Data?
}
