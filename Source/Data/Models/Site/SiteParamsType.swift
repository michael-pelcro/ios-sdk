//
//  SiteParamsType.swift
//  Pelcro
//
//  Created by Denys Trush on 6/14/19.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

enum SiteParamsType {
    case siteID(String?)
}
