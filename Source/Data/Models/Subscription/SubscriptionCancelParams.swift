//
//  SubscriptionCancelParams.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

struct SubscriptionCancelParams {
    let baseParams: SubscriptionBaseParams
    let subscriptionID: String
}
