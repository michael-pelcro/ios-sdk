//
//  SubscriptionParams.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

struct SubscriptionCreateParams {
    let baseParams: SubscriptionBaseParams
    let planID: String
    let couponCode: String
    let appleIAPReceipt: String
}
