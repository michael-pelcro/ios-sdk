//
//  SubscriptionReactivateParams.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

struct SubscriptionReactivateParams {
    let baseParams: SubscriptionBaseParams
    let subscriptionID: String
}
