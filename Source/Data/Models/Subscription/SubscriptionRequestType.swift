//
//  SubscriptionRequestType.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

enum SubscriptionRequestType {
    case create
    case cancel
    case reactivate
}
