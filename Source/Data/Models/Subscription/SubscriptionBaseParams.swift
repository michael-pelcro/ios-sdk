//
//  SubscriptionBaseParams.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

struct SubscriptionBaseParams {
    let siteID: String
    let accountID: String
    let authToken: String
    let language: String
}
