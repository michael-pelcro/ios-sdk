//
//  SubscriptionParamsType.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

enum SubscriptionParamsType {
    case create(SubscriptionCreateParams)
    case cancel(SubscriptionCancelParams)
    case reactivate(SubscriptionReactivateParams)
}
