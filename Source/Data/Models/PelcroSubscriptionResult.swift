//
//  PelcroSubscriptionResult.swift
//  Pelcro
//
//  Created by Denys Trush on 6/24/19.
//

import Foundation

public class PelcroSubscriptionResult {
    public var data: [String: Any]?
    public var error: PurchaseError?
}
