//
//  RefreshParams.swift
//  Pelcro
//
//  Created by Denys Trush on 6/18/19.
//

import Foundation

struct RefreshParams {
    let siteID: String
    let accountID: String
    let authToken: String
}
