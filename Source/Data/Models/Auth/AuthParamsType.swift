//
//  AuthParamsType.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

enum AuthParamsType {
    case register(RegisterParams)
    case login(AuthParams)
    case refresh(RefreshParams)
}
