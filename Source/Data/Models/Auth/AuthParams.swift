//
//  AuthParams.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct AuthParams {
    let siteID: String
    let email: String
    let password: String
}
