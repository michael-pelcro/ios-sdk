//
//  PelcroResult.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

public class PelcroResult {
    public var data: [String: Any]?
    public var error: Error?
}
