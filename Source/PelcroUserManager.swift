//
//  PelcroUserManager.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

public class PelcroUserManager {
    
    public private(set) var user: [String: Any]?
    
    public typealias AuthResultCompletion = ((PelcroResult) -> Void)
    
    static let language = "en"
    static let errors = "errors"
    static let data = "data"
    static let authToken = "auth_token"
    
    private let requestDataSourceFactory: AnyRequestDataSourceFactory<AuthRequestType, AuthParamsType>
    private let requestFactory: RequestFactory
    private let sendRequestTask: AnyURLRequestAsyncTask<[String: Any]>
    
    private var builder: PelcroAuthenticatorComponentsBuilder?
    
    public convenience init() {
        let builder = PelcroAuthenticatorComponentsBuilder()
        let dataSourceFactory = builder.buildURLRequestDataSourceFactory()
        let sendRequestTask = builder.buildSendURLRequestTask()
        self.init(requestDataSourceFactory: dataSourceFactory,
                  requestFactory: builder.requestFactory,
                  sendRequestTask: sendRequestTask)
        self.builder = builder
    }
    
    init<DataSourceFactory: RequestDataSourceFactory,
        StartRequestTask: URLRequestAsyncTask>
        (requestDataSourceFactory: DataSourceFactory,
         requestFactory: RequestFactory,
         sendRequestTask: StartRequestTask)
        where DataSourceFactory.RequestType == AuthRequestType,
        DataSourceFactory.BodyInfoType == AuthParamsType,
        StartRequestTask.ResultType == [String: Any] {
            self.requestDataSourceFactory = requestDataSourceFactory.erased
            self.requestFactory = requestFactory
            self.sendRequestTask = sendRequestTask.erased
    }
    
    public func register(with email: String,
                         password: String,
                         completion: AuthResultCompletion?) {
        let accountID = Pelcro.shared.accountID ?? ""
        let registerParams = RegisterParams(authParams: self.getAuthParams(email, password),
                                            accountID: accountID,
                                            language: PelcroUserManager.language)
        let requestDataSource = try! self.requestDataSourceFactory.requestDataSource(for: .register, bodyInfo: .register(registerParams))
        self.sendRequest(requestDataSource: requestDataSource, completion: completion)
    }
    
    public func login(with email: String,
                      password: String,
                      completion: AuthResultCompletion?) {
        let requestDataSource = try! requestDataSourceFactory.requestDataSource(for: .login, bodyInfo: .login(getAuthParams(email, password)))
        sendRequest(requestDataSource: requestDataSource, completion: completion)
    }
    
    public func refresh(completion: AuthResultCompletion?) {
        let siteID = Pelcro.shared.siteID ?? ""
        let accountID = Pelcro.shared.accountID ?? ""
        let authToken = Pelcro.shared.authToken ?? ""
        let refreshParams = RefreshParams(siteID: siteID, accountID: accountID, authToken: authToken)
        let requestDataSource = try! requestDataSourceFactory.requestDataSource(for: .refresh, bodyInfo: .refresh(refreshParams))
        sendRequest(requestDataSource: requestDataSource, completion: completion)
    }
    
    public func logout() {
        Pelcro.shared.updateAuthToken(nil)
        self.user = nil
    }
    
    private func sendRequest(requestDataSource: URLRequestDataSource,
                             completion: AuthResultCompletion?) {
        let request = try! requestFactory.requestWith(dataSource: requestDataSource)
        sendRequestTask.run(urlRequest: request) { data, error in
            let result = PelcroResult()
            result.error = error
            result.data = data
            
            let errorDict = data?[PelcroUserManager.errors] as? [String: Any]
            if error == nil, errorDict == nil, let userData = data {
                self.user = userData
            }
            if let parentDict = data?[PelcroUserManager.data] as? [String: Any] {
                if let authToken = parentDict[PelcroUserManager.authToken] as? String {
                    Pelcro.shared.updateAuthToken(authToken)
                }
            }
            
            completion?(result)
        }
    }
    
    private func getAuthParams(_ email: String, _ password: String) -> AuthParams {
        let siteID = Pelcro.shared.siteID ?? ""
        return AuthParams(siteID: siteID, email: email, password: password)
    }
}
