//
//  PelcroSubscriptionManager.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

@available(iOS 10.0, *)
public class PelcroSubscriptionManager {
    
    public private(set) var productsIdentifiers: Set<String>
    
    public typealias SubscriptionResultCompletion = ((PelcroSubscriptionResult) -> Void)
    public typealias IsSubscribedResultCompletion = ((Bool) -> Void)
    
    static let language = "en"
    
    private let requestDataSourceFactory: AnyRequestDataSourceFactory<SubscriptionRequestType, SubscriptionParamsType>
    private let requestFactory: RequestFactory
    private let sendRequestTask: AnyURLRequestAsyncTask<[String: Any]>
    
    private var purchaseController: PurchaseController?
    private var builder: PelcroSubscriptionComponentsBuilder?
    
    private var isSubscribedResultCompletion: IsSubscribedResultCompletion?
    private var subscriptionResultCompletion: SubscriptionResultCompletion?
    private var site: [String: Any]?
    private var user: [String: Any]?
    
    public convenience init(productsIdentifiers: Set<String>) {
        let builder = PelcroSubscriptionComponentsBuilder()
        let dataSourceFactory = builder.buildURLRequestDataSourceFactory()
        let sendRequestTask = builder.buildSendURLRequestTask()
        self.init(requestDataSourceFactory: dataSourceFactory,
                  requestFactory: builder.requestFactory,
                  sendRequestTask: sendRequestTask,
                  productsIdentifiers: productsIdentifiers)
        self.builder = builder
        self.purchaseController = PurchaseController(stateHandler: self)
        self.purchaseController?.completeTransactions()
    }
    
    init<DataSourceFactory: RequestDataSourceFactory,
        StartRequestTask: URLRequestAsyncTask>
        (requestDataSourceFactory: DataSourceFactory,
         requestFactory: RequestFactory,
         sendRequestTask: StartRequestTask,
         productsIdentifiers: Set<String>)
        where DataSourceFactory.RequestType == SubscriptionRequestType,
        DataSourceFactory.BodyInfoType == SubscriptionParamsType,
        StartRequestTask.ResultType == [String: Any] {
            self.requestDataSourceFactory = requestDataSourceFactory.erased
            self.requestFactory = requestFactory
            self.sendRequestTask = sendRequestTask.erased
            self.productsIdentifiers = productsIdentifiers
    }
    
    public func createSubscription(with planID: String,
                                   appleProductID: String,
                                   couponCode: String?,
                                   completion: SubscriptionResultCompletion?) {
        self.subscriptionResultCompletion = completion
        self.purchaseController?.purchase(with: appleProductID, planID: planID, couponCode: couponCode, completion: completion, atomically: false)
    }
    
    private func create(with planID: String,
                        couponID: String,
                        receipt: String,
                        completion: SubscriptionResultCompletion?) {
        let createParams = SubscriptionCreateParams(baseParams: self.getSubscriptionBaseParams(),
                                                    planID: planID,
                                                    couponCode: couponID,
                                                    appleIAPReceipt: receipt)
        let requestDataSource = try! self.requestDataSourceFactory.requestDataSource(for: .create, bodyInfo: .create(createParams))
        self.sendRequest(requestDataSource: requestDataSource, completion: completion)
    }
    
    public func cancelSubscription(with subscriptionID: String,
                                   completion: SubscriptionResultCompletion?) {
        let cancelParams = SubscriptionCancelParams(baseParams: self.getSubscriptionBaseParams(), subscriptionID: subscriptionID)
        let requestDataSource = try! requestDataSourceFactory.requestDataSource(for: .cancel, bodyInfo: .cancel(cancelParams))
        sendRequest(requestDataSource: requestDataSource, completion: completion)
    }
    
    public func reactivateSubscription(with subscriptionID: String,
                                       completion: SubscriptionResultCompletion?) {
        let reactivateParams = SubscriptionReactivateParams(baseParams: self.getSubscriptionBaseParams(), subscriptionID: subscriptionID)
        let requestDataSource = try! requestDataSourceFactory.requestDataSource(for: .reactivate, bodyInfo: .reactivate(reactivateParams))
        sendRequest(requestDataSource: requestDataSource, completion: completion)
    }
    
    public func isSubscribedToSite(user: [String: Any], completion: IsSubscribedResultCompletion?) {
        self.isSubscribedResultCompletion = completion
        self.user = user
        self.purchaseController?.verifyReceipt(sharedSecret: Pelcro.shared.appSharedSecret ?? "", isSandbox: Pelcro.shared.isSandbox)
    }
    
    private func sendRequest(requestDataSource: URLRequestDataSource,
                             completion: SubscriptionResultCompletion?) {
        let request = try! requestFactory.requestWith(dataSource: requestDataSource)
        sendRequestTask.run(urlRequest: request) { data, error in
            let result = PelcroSubscriptionResult()
            result.error = error?.asPurchaseError()
            result.data = data
            
            completion?(result)
        }
    }
    
    private func getSubscriptionBaseParams() -> SubscriptionBaseParams {
        let siteID = Pelcro.shared.siteID ?? ""
        let accountID = Pelcro.shared.accountID ?? ""
        let authToken = Pelcro.shared.authToken ?? ""
        return SubscriptionBaseParams(siteID: siteID, accountID: accountID, authToken: authToken, language: PelcroSubscriptionManager.language)
    }
    
    private func isSubscribedToSiteCalculation(localPurchasedProducts: [PurchaseItem]?) {
        guard let purchasedProducts = localPurchasedProducts, purchasedProducts.count > 0 else {
            self.isSubscribedResultCompletion?(self.checkSubscriptionToSite(with: self.user))
            return
        }
        
        for productID in self.productsIdentifiers {
            let filterResult = purchasedProducts.filter { (item: PurchaseItem) -> Bool in
                return item.productId == productID
            }
            let sorted = filterResult.sorted(by: { (lo, ro) -> Bool in
                guard let transactionDateLo = lo.transaction.transactionDate, let transactionDateRo = ro.transaction.transactionDate else {
                    return false
                }
                return transactionDateLo.timeIntervalSince1970 > transactionDateRo.timeIntervalSince1970
            })
            
            if sorted.first == nil {
                let subscriptions = self.getSubscriptionsFromUser(user: self.user)
                for subscription in subscriptions {
                    if let plan = subscription["plan"] as? [String: Any],
                        let appleProductID = plan["apple_product_id"] as? String,
                        productID == appleProductID,
                        let subscriptionID = subscription["id"] as? String {
                        self.cancelSubscription(with: subscriptionID) { (result: PelcroSubscriptionResult) in
                            print(result.data ?? "No data returned")
                            print(result.error ?? "No error returned")
                        }
                    }
                }
            }
        }
        
        self.isSubscribedResultCompletion?(self.checkSubscriptionToSite(with: self.user))
    }
    
    private func checkSubscriptionToSite(with user: [String: Any]?) -> Bool {
        let subscriptions = self.getSubscriptionsFromUser(user: user)
        return subscriptions.count > 0 && subscriptions.first?.count ?? 0 > 0
    }
    
    private func getSubscriptionsFromUser(user: [String: Any]?) -> [[String: Any]] {
        guard let user = self.user, let parentDict = user["data"] as? [String: Any], let subscriptions = parentDict["subscriptions"] as? [[String: Any]] else {
            return [[:]]
        }
        return subscriptions
    }
    
}

@available(iOS 10.0, *)
extension PelcroSubscriptionManager: PurchaseStateHandler {
    func update(newState: PurchaseActionState, from state: PurchaseActionState) {
        switch (state, newState) {
            
        case ( .loading, .finish(let result)):
            switch (result) {
            case .purchaseSuccess(let item):
                guard let receiptData = item.receiptData else {
                    let result = PelcroSubscriptionResult()
                    result.error = PurchaseError.noReceiptData
                    item.completion?(result)
                    return
                }
                let receipt = String(data: receiptData, encoding: .utf8)
                try? item.completeOriginalTransaction()
                item.completeTransaction()
                
                self.create(with: item.planID ?? "", couponID: item.couponCode ?? "", receipt: receipt ?? "", completion: item.completion)
            case .receiptValidationSuccess:
                self.purchaseController?.decodeIfPresent(sessionReceipt: self.purchaseController?.sessionReceipt)
            case .receiptDecodeSuccess( _):
                self.purchaseController?.retrieve(products: self.productsIdentifiers)
            case .purchaseSyncronizationSuccess:
                let localPurchasedProducts = try? self.purchaseController?.localPurschasedProducts(by: { _ in return true })
                self.isSubscribedToSiteCalculation(localPurchasedProducts: localPurchasedProducts as? [PurchaseItem])
            case .error(let error):
                print("--- Error occured: \(error)")
                let result = PelcroSubscriptionResult()
                result.error = error
                self.subscriptionResultCompletion?(result)
            default:
                print("--- Moved to state: \(newState)")
            }
            
        case ( .finish( _), .finish(let result)):
            switch (result) {
            case .retrieveSuccess:
                self.purchaseController?.synchronizeLocalPurchasesFromReceipt()
            case .retrieveSuccessInvalidProducts:
                self.purchaseController?.synchronizeLocalPurchasesFromReceipt()
            default:
                print("--- Moved to state: \(newState)")
            }
            
        default: print("--- State changing to \(newState)")
        }
    }
}
