//
//  AnyDataProvider.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct AnyDataProvider<T>: DataProvider {
    typealias DataType = T
    
    var data: T
    
    init<Provider: DataProvider>(provider: Provider) where Provider.DataType == T {
        data = provider.data
    }
}
