//
//  AnyDataConverter.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct AnyDataConverter<D, C>: DataConverter {
    typealias DataToConvertType = D
    typealias ConvertedType = C
    
    private var _converted: (D) -> C
    
    init<Converter: DataConverter>
        (converter: Converter)
        where Converter.DataToConvertType == D,
        Converter.ConvertedType == C {
        _converted = converter.converted
    }
    
    func converted(data: D) -> C {
        return _converted(data)
    }
}
