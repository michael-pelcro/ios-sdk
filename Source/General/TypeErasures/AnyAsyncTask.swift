//
//  AnyAsyncTask.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

struct AnyAsyncTask<Result>: AsyncTask {
    typealias ResultType = Result
    
    typealias CompletionType = ((Result?, Error?) -> Void)?
    
    private let _runWith: (CompletionType) -> Void
    
    init<Task: AsyncTask>(task: Task) where Task.ResultType == Result {
        _runWith = task.runWith
    }
    
    func runWith(completion: CompletionType) {
        _runWith(completion)
    }
}
