//
//  StringProvider.swift
//  Pelcro
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

protocol DataProvider {
    associatedtype DataType
    var data: DataType { get }
}

extension DataProvider {
    var erased: AnyDataProvider<DataType> {
        return AnyDataProvider(provider: self)
    }
}
