//
//  PelcroSubscriptionComponentsBuilder.swift
//  Pelcro
//
//  Created by Denys Trush on 6/19/19.
//

import Foundation

class PelcroSubscriptionComponentsBuilder {
    
    private var baseURLProvider: HardCodedBaseURLProvider?
    private var bodyInfoConverter: SubscriptionParamsToDataConverter?
    private var dataSourceFactory: SubscriptionRequestDataSourceFactory?
    private var sendRequestTask: SendURLRequestTask?
    private var sendRequestAdapterTask: SendURLRequestAdapterTask?
    
    let requestFactory = URLRequestFactory()
    
    func buildURLRequestDataSourceFactory() -> SubscriptionRequestDataSourceFactory {
        let baseURLProvider = HardCodedBaseURLProvider()
        let bodyInfoConverter = SubscriptionParamsToDataConverter(toDictionaryConverter: SubscriptionParamsToDictionaryConverter(), toDataConverter: DictionaryToDataConverter())
        let dataSourceFactory = SubscriptionRequestDataSourceFactory(baseURLProvider: baseURLProvider, bodyInfoConverter: bodyInfoConverter)
        self.dataSourceFactory = dataSourceFactory
        self.baseURLProvider = baseURLProvider
        self.bodyInfoConverter = bodyInfoConverter
        return dataSourceFactory
    }
    
    func buildSendURLRequestTask() -> SendURLRequestAdapterTask {
        let sendRequestTask = SendURLRequestTask(urlSession: URLSession.shared)
        let sendRequestAdapterTask = SendURLRequestAdapterTask(urlRequestTask: sendRequestTask)
        self.sendRequestTask = sendRequestTask
        self.sendRequestAdapterTask = sendRequestAdapterTask
        return sendRequestAdapterTask
    }
    
}
