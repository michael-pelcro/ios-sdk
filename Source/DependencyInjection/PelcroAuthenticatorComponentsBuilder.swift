//
//  PelcroAuthenticatorComponentsBuilder.swift
//  Pelcro
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

class PelcroAuthenticatorComponentsBuilder {
    
    private var baseURLProvider: HardCodedBaseURLProvider?
    private var bodyInfoConverter: AuthParamsToDataConverter?
    private var dataSourceFactory: AuthRequestDataSourceFactory?
    private var sendRequestTask: SendURLRequestTask?
    private var sendRequestAdapterTask: SendURLRequestAdapterTask?
    
    let requestFactory = URLRequestFactory()

    func buildURLRequestDataSourceFactory() -> AuthRequestDataSourceFactory {
        let baseURLProvider = HardCodedBaseURLProvider()
        let bodyInfoConverter = AuthParamsToDataConverter(toDictionaryConverter: AuthParamsToDictionaryConverter(), toDataConverter: DictionaryToDataConverter())
        let dataSourceFactory = AuthRequestDataSourceFactory(baseURLProvider: baseURLProvider, bodyInfoConverter: bodyInfoConverter)
        self.dataSourceFactory = dataSourceFactory
        self.baseURLProvider = baseURLProvider
        self.bodyInfoConverter = bodyInfoConverter
        return dataSourceFactory
    }
    
    func buildSendURLRequestTask() -> SendURLRequestAdapterTask {
        let sendRequestTask = SendURLRequestTask(urlSession: URLSession.shared)
        let sendRequestAdapterTask = SendURLRequestAdapterTask(urlRequestTask: sendRequestTask)
        self.sendRequestTask = sendRequestTask
        self.sendRequestAdapterTask = sendRequestAdapterTask
        return sendRequestAdapterTask
    }

}
