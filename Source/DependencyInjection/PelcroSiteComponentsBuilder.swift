//
//  PelcroSiteComponentsBuilder.swift
//  Pelcro
//
//  Created by Denys Trush on 6/14/19.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

class PelcroSiteComponentsBuilder {
    
    private var baseURLProvider: HardCodedBaseURLProvider?
    private var bodyInfoConverter: SiteParamsToDataConverter?
    private var dataSourceFactory: SiteRequestDataSourceFactory?
    private var sendRequestTask: SendURLRequestTask?
    private var sendRequestAdapterTask: SendURLRequestAdapterTask?
    
    let requestFactory = URLRequestFactory()
    
    func buildURLRequestDataSourceFactory() -> SiteRequestDataSourceFactory {
        let baseURLProvider = HardCodedBaseURLProvider()
        let bodyInfoConverter = SiteParamsToDataConverter(toDictionaryConverter: SiteParamsToDictionaryConverter(), toDataConverter: DictionaryToDataConverter())
        let dataSourceFactory = SiteRequestDataSourceFactory(baseURLProvider: baseURLProvider, bodyInfoConverter: bodyInfoConverter)
        self.dataSourceFactory = dataSourceFactory
        self.baseURLProvider = baseURLProvider
        self.bodyInfoConverter = bodyInfoConverter
        return dataSourceFactory
    }
    
    func buildSendURLRequestTask() -> SendURLRequestAdapterTask {
        let sendRequestTask = SendURLRequestTask(urlSession: URLSession.shared)
        let sendRequestAdapterTask = SendURLRequestAdapterTask(urlRequestTask: sendRequestTask)
        self.sendRequestTask = sendRequestTask
        self.sendRequestAdapterTask = sendRequestAdapterTask
        return sendRequestAdapterTask
    }

}
