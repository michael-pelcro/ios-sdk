//
//  Pelcro.swift
//  Pelcro
//
//  Created by Corneliu on 08/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

public class Pelcro {
    
    public static let shared = Pelcro()
    public var siteID: String?
    public var accountID: String?
    public var appSharedSecret: String?
    
    public var isSandbox: Bool = true
    public var isStaging: Bool = true
    
    public private(set) var authToken: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: PelcroUserManager.authToken)
        }
        get {
            return UserDefaults.standard.string(forKey: PelcroUserManager.authToken)
        }
    }
    
    internal func updateAuthToken(_ token: String?) {
        self.authToken = token
    }
    
    private init() {}
}
