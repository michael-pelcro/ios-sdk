//
//  PelcroSiteManager.swift
//  Pelcro
//
//  Created by Denys Trush on 6/14/19.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

public class PelcroSiteManager {
    
    public private(set) var site: [String: Any]?
    
    public typealias SiteResultCompletion = ((PelcroResult) -> Void)
    
    static let language = "en"
    
    private let requestDataSourceFactory: AnyRequestDataSourceFactory<SiteRequestType, SiteParamsType>
    private let requestFactory: RequestFactory
    private let sendRequestTask: AnyURLRequestAsyncTask<[String: Any]>
    
    private var builder: PelcroSiteComponentsBuilder?
    
    public convenience init() {
        let builder = PelcroSiteComponentsBuilder()
        let dataSourceFactory = builder.buildURLRequestDataSourceFactory()
        let sendRequestTask = builder.buildSendURLRequestTask()
        self.init(requestDataSourceFactory: dataSourceFactory,
                  requestFactory: builder.requestFactory,
                  sendRequestTask: sendRequestTask)
        self.builder = builder
    }
    
    init<DataSourceFactory: RequestDataSourceFactory,
        StartRequestTask: URLRequestAsyncTask>
        (requestDataSourceFactory: DataSourceFactory,
         requestFactory: RequestFactory,
         sendRequestTask: StartRequestTask)
        where DataSourceFactory.RequestType == SiteRequestType,
        DataSourceFactory.BodyInfoType == SiteParamsType,
        StartRequestTask.ResultType == [String: Any] {
            self.requestDataSourceFactory = requestDataSourceFactory.erased
            self.requestFactory = requestFactory
            self.sendRequestTask = sendRequestTask.erased
    }
    
    public func getSite(with id: String, completion: SiteResultCompletion?) {
        let siteID = Pelcro.shared.siteID ?? ""
        let requestDataSource = try! requestDataSourceFactory.requestDataSource(for: .getSiteInfo, bodyInfo: .siteID(siteID))
        sendRequest(requestDataSource: requestDataSource, completion: completion)
    }
    
    private func sendRequest(requestDataSource: URLRequestDataSource,
                             completion: SiteResultCompletion?) {
        let request = try! requestFactory.requestWith(dataSource: requestDataSource)
        sendRequestTask.run(urlRequest: request) { data, error in
            let result = PelcroResult()
            result.error = error
            result.data = data
            
            let errorDict = data?[PelcroUserManager.errors] as? [String: Any]
            if error == nil, errorDict == nil, let siteData = data {
                self.site = siteData
            }
            
            completion?(result)
        }
    }

}
