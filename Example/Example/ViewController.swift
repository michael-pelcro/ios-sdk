//
//  ViewController.swift
//  Example
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import UIKit
import Pelcro

class ViewController: UIViewController {
    
    lazy var siteManager = PelcroSiteManager()
    lazy var userManager = PelcroUserManager()
    lazy var subscriptionManager = PelcroSubscriptionManager(productsIdentifiers: ["<YOUR APPLE PRODUCT ID>", "<YOUR APPLE PRODUCT ID>"])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Pelcro.shared.siteID = "<YOUR SITE ID>"
        Pelcro.shared.accountID = "<YOUR ACCOUNT ID>"
        Pelcro.shared.appSharedSecret = "<YOUR APPLE APP SHARED SECRET>"
        Pelcro.shared.isSandbox = true
        Pelcro.shared.isStaging = true
        
        self.userManager.logout()
        
        self.userManager.register(with: "<YOUR EMAIL>", password: "<YOUR PASSWORD>") { (result: PelcroResult) in
            print(result.data ?? "No data returned")
            print(result.error ?? "No error returned")
        }
        
        if Pelcro.shared.authToken != nil {
            self.userManager.refresh { (result: PelcroResult) in
                print(result.data ?? "No data returned")
                print(result.error ?? "No error returned")
                
                self.siteManager.getSite(with: "17") { (result: PelcroResult) in
                    print(result.data ?? "No data returned")
                    print(result.error ?? "No error returned")
                    
                    self.subscriptionManager.isSubscribedToSite(user: self.userManager.user ?? [:], completion: { (result: Bool) in
                        print(result)
                    })
                    
                    self.subscriptionManager.createSubscription(with: "<YOUR PLAN ID>", appleProductID: "<YOUR APPLE PRODUCT ID>", couponCode: nil, completion: { (result: PelcroSubscriptionResult) in
                        print(result.data ?? "No data returned")
                        print(result.error ?? "No error returned")
                    })
                }
            }
        } else {
            self.userManager.login(with: "<YOUR EMAIL>", password: "<YOUR PASSWORD>") { (result: PelcroResult) in
                print(result.data ?? "No data returned")
                print(result.error ?? "No error returned")
            }
        }
    }
    
}

