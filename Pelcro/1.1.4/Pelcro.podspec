Pod::Spec.new do |s|
  s.name = 'Pelcro'
  s.version = '1.1.4'
  s.license = 'MIT'
  s.summary = 'Your convenient SDK to access the Pelcro platform.'
  s.homepage = 'https://bitbucket.org/michael-pelcro/ios-sdk/'
  s.authors = { 'Pelcro' => 'help@pelcro.com' }
  s.source = { :git => 'https://bitbucket.org/michael-pelcro/ios-sdk.git', :tag => s.version }
  s.documentation_url = 'https://bitbucket.org/michael-pelcro/ios-sdk/'
  s.ios.deployment_target = '10.0'
  s.swift_version = '4.2'
  s.source_files = 'Source/**/*'
  s.exclude_files = ['Source/Pelcro.h', 'Source/Info.plist']
  s.frameworks = 'StoreKit'
  s.dependency 'SwiftyStoreKit'
end
