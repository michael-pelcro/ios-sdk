//
//  ToDataConverterStub.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

struct ToDataConverterStub: DataConverter {
    typealias DataToConvertType = [String: Any]?
    typealias ConvertedType = Data?
    
    let returnedData = Data()
    
    func converted(data: [String : Any]?) -> Data? {
        return returnedData
    }
}
