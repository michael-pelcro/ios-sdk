//
//  StringDataProviderStub.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

struct StringDataProviderStub: DataProvider {
    typealias DataType = String
    var data: String {
        return stringToProvide
    }
    let stringToProvide: String
}
