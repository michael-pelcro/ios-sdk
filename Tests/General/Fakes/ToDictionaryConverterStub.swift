//
//  ToDictionaryConverterStub.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

struct ToDictionaryConverterStub: DataConverter {
    typealias DataToConvertType = AuthParamsType
    typealias ConvertedType = [String: Any]?
    
    let returnedData: ConvertedType = ["key": "value", "key2": "value2", "keyInt": 34]
    
    func converted(data: AuthParamsType) -> [String : Any]? {
        return returnedData
    }
}
