//
//  AuthParamDataConverterStub.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

struct AuthParamDataConverterStub: DataConverter {
    typealias DataToConvertType = AuthParamsType
    typealias ConvertedType = Data?
    
    let registerAuthDataToReturn = Data()
    
    func converted(data: AuthParamsType) -> Data? {
        return registerAuthDataToReturn
    }
}
