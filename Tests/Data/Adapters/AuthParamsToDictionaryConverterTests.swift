//
//  AuthParamsToDictionaryConverterTests.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import XCTest
@testable import Pelcro

class AuthParamsToDictionaryConverterTests: XCTestCase {

    func test_WhenConvertingSiteInfoParams_ReturnNil() {
        XCTAssertNil(resultFromConverting(paramType: .siteID("")))
    }
    
    func test_WhenConvertingLoginParams_ReturnCorrectSiteID() {
        let params = FakeParams.getTestAuthParams()
        XCTAssertEqual(valueFromConvertedLoginParamsWith(key: "site_id", params: params),
                       params.siteID)
    }
    
    func test_WhenConvertingLoginParams_ReturnCorrectEmail() {
        let params = FakeParams.getTestAuthParams()
        XCTAssertEqual(valueFromConvertedLoginParamsWith(key: "email", params: params),
                       params.email)
    }
    
    func test_WhenConvertingLoginParams_ReturnCorrectPassword() {
        let params = FakeParams.getTestAuthParams()
        XCTAssertEqual(valueFromConvertedLoginParamsWith(key: "password", params: params),
                       params.password)
    }
    
    func test_WhenConvertingRegisterParams_ReturnCorrectSiteID() {
        let params = FakeParams.getTestRegisterParams()
        XCTAssertEqual(valueFromConvertedRegisterParamsWith(key: "site_id", params: params),
                       params.authParams.siteID)
    }
    
    func test_WhenConvertingRegisterParams_ReturnCorrectEmail() {
        let params = FakeParams.getTestRegisterParams()
        XCTAssertEqual(valueFromConvertedRegisterParamsWith(key: "email", params: params),
                       params.authParams.email)
    }
    
    func test_WhenConvertingRegisterParams_ReturnCorrectPassword() {
        let params = FakeParams.getTestRegisterParams()
        XCTAssertEqual(valueFromConvertedRegisterParamsWith(key: "password", params: params),
                       params.authParams.password)
    }
    
    func test_WhenConvertingRegisterParams_ReturnCorrectLanguage() {
        let params = FakeParams.getTestRegisterParams()
        XCTAssertEqual(valueFromConvertedRegisterParamsWith(key: "language", params: params),
                       params.language)
    }
    
    func test_WhenConvertingRegisterParams_ReturnCorrectAccountID() {
        let params = FakeParams.getTestRegisterParams()
        XCTAssertEqual(valueFromConvertedRegisterParamsWith(key: "account_id", params: params),
                       params.accountID)
    }
    
    //MARK: Helper functions
    private func valueFromConvertedLoginParamsWith(key: String, params: AuthParams) -> String {
        let result = resultFromConverting(paramType: .login(params))
        return result![key] as! String
    }
    
    private func valueFromConvertedRegisterParamsWith(key: String, params: RegisterParams) -> String {
        let result = resultFromConverting(paramType: .register(params))
        return result![key] as! String
    }
    
    private func resultFromConverting(paramType: AuthParamsType) -> [String: Any]? {
        let sut = AuthParamsToDictionaryConverter()
        return sut.converted(data: paramType)
    }
}
