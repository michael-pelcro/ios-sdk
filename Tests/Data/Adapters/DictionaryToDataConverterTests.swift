//
//  DictionaryToDataConverterTests.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import XCTest
@testable import Pelcro

class DictionaryToDataConverterTests: XCTestCase {
    func test_ConvertingNilDictionary_ConvertsToNil() {
        let sut = DictionaryToDataConverter()
        XCTAssertNil(sut.converted(data: nil))
    }
    
    func test_WhenConvertingADictionary_ReturnJSONData() {
        let sut = DictionaryToDataConverter()
        let testDictionary: [String: Any] = ["key": "value", "keyInt": 24]
        let expectedData = try! JSONSerialization.data(withJSONObject: testDictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
        XCTAssertEqual(sut.converted(data: testDictionary), expectedData)
    }
}
