//
//  AuthParamsToDataConverterTests.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import XCTest
@testable import Pelcro

class AuthParamsToDataConverterTests: XCTestCase {
    
    func test_WhenAskingToConvert_UseConvertersFromInit() {
        let toDictionaryConverterStub = ToDictionaryConverterStub()
        let toDataConverterStub = ToDataConverterStub()
        let sut = AuthParamsToDataConverter(toDictionaryConverter: toDictionaryConverterStub,
                                            toDataConverter: toDataConverterStub)
        let convertedData = sut.converted(data: .siteID(""))
        XCTAssertEqual(convertedData, toDataConverterStub.returnedData)
    }
}
