//
//  AuthRequestDataSourceFactoryTests.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import XCTest
@testable import Pelcro

class AuthRequestDataSourceFactoryTests: XCTestCase {
    
    private var sut: AuthRequestDataSourceFactory!
    private var paramsConverterStub: AuthParamDataConverterStub!
    private let fakeBaseURL = "Fake base URL"
    
    override func setUp() {
        super.setUp()
        let baseURLProviderStub = StringDataProviderStub(stringToProvide: fakeBaseURL)
        paramsConverterStub = AuthParamDataConverterStub()
        sut = AuthRequestDataSourceFactory(baseURLProvider: baseURLProviderStub, bodyInfoConverter: paramsConverterStub)
    }
    
    override func tearDown() {
        sut = nil
        paramsConverterStub = nil
        super.tearDown()
    }
    
    //MARK: Get site info data source tests
    func test_WhenSiteIDIsMissing_ReturnURLWithoutSiteIDParameter() {
        let expectedValue = "\(fakeBaseURL)/site"
        let result = try! sut.requestDataSource(for: .getSiteInfo,
                                                bodyInfo: .siteID(nil))
        XCTAssertEqual(result.urlString, expectedValue,
                       "Should provide correct url for get site info request")
    }

    func test_WhenCreatingGetSiteInfo_ReturnAppropriateURL() {
        let expectedValue = "\(fakeBaseURL)/site?site_id=\(FakeParams.fakeSiteID)"
        XCTAssertEqual(getSiteInfoDataSource().urlString, expectedValue,
                       "Should provide correct url for get site info request")
    }
    
    func test_WhenCreatingGetSiteInfo_ReturnAppropriateHTTPMethod() {
        XCTAssertEqual(getSiteInfoDataSource().httpMethod, .get,
                       "Should provide correct HTTP method for get site info request")
    }
    
    func test_WhenCreatingGetSiteInfo_ReturnNoHeaderValues() {
        XCTAssertEqual(getSiteInfoDataSource().headerValues.count, 0,
                       "Should provide no header values for get site info request")
    }
    
    func test_WhenCreatingGetSiteInfo_ReturnCorrectBody() {
        XCTAssertEqual(getSiteInfoDataSource().httpBody, paramsConverterStub.registerAuthDataToReturn,
                     "Should provide nil HTTP body for get site info request")
    }
    
    //MARK: Register data source tests
    func test_WhenCreatingRegisterDataSource_ReturnAppropriateURL() {
        XCTAssertEqual(getRegisterDataSource().urlString, "\(fakeBaseURL)/auth/register",
            "Should provide correct url for register url request data source")
    }
    
    func test_WhenCreatingRegisterDataSource_ReturnPostHTTPMethod() {
        XCTAssertEqual(getRegisterDataSource().httpMethod, .post,
            "Should provide correct http method for register url request data source")
    }
    
    func test_WhenCreatingRegisterDataSource_ReturnCorrectAcceptHeaderField() {
        XCTAssertEqual(getRegisterDataSource().headerValues[.accept], "application/json",
                       "Should provide correct `Accept` header value for register url request data source")
    }
    
    func test_WhenCreatingRegisterDataSource_ReturnCorrectBody() {
        XCTAssertEqual(getRegisterDataSource().httpBody, paramsConverterStub.registerAuthDataToReturn,
                       "Should provide correct `Accept` header value for register url request data source")
    }
    
    //MARK: Login data source tests
    func test_WhenCreatingLoginDataSource_ReturnAppropriateURL() {
        XCTAssertEqual(getLoginDataSource().urlString, "\(fakeBaseURL)/auth/login",
            "Should provide correct url for register url request data source")
    }
    
    func test_WhenCreatingLoginDataSource_ReturnPostHTTPMethod() {
        XCTAssertEqual(getLoginDataSource().httpMethod, .post,
                       "Should provide correct http method for register url request data source")
    }
    
    func test_WhenCreatingLoginDataSource_ReturnCorrectAcceptHeaderField() {
        XCTAssertEqual(getLoginDataSource().headerValues[.accept], "application/json",
                       "Should provide correct `Accept` header value for register url request data source")
    }
    
    func test_WhenCreatingLoginDataSource_ReturnCorrectContentTypeHeaderField() {
        XCTAssertEqual(getLoginDataSource().headerValues[.contentType], "application/json",
                       "Should provide correct `Content-Type` header value for register url request data source")
    }
    
    func test_WhenCreatingLoginDataSource_ReturnCorrectBody() {
        XCTAssertEqual(getLoginDataSource().httpBody, paramsConverterStub.registerAuthDataToReturn,
                       "Should provide correct `Accept` header value for register url request data source")
    }
    
    //MARK: Params are not correct tests
    
    func test_WhenCreatingGetSiteInfoWithWrongBodyInfo_ThrowIncorrectBodyInfoError() {
        assertErrorThrownWhenSendingWrongParams(for: .getSiteInfo,
                                                bodyInfo: .register(FakeParams.getTestRegisterParams()))
        assertErrorThrownWhenSendingWrongParams(for: .getSiteInfo,
                                                bodyInfo: .login(FakeParams.getTestAuthParams()))
    }
    
    func test_WhenCreatingRegisterDataSourceWithWrongBodyInfo_ThrowIncorrectBodyInfoError() {
        assertErrorThrownWhenSendingWrongParams(for: .register,
                                                bodyInfo: .siteID(FakeParams.fakeSiteID))
        assertErrorThrownWhenSendingWrongParams(for: .register,
                                                bodyInfo: .login(FakeParams.getTestAuthParams()))
    }
    
    func test_WhenCreatingLoginDataSourceWithWrongBodyInfo_ThrowIncorrectBodyInfoError() {
        assertErrorThrownWhenSendingWrongParams(for: .login,
                                                bodyInfo: .siteID(FakeParams.fakeSiteID))
        assertErrorThrownWhenSendingWrongParams(for: .login,
                                                bodyInfo: .register(FakeParams.getTestRegisterParams()))
    }
    
    //MARK: Helper functions
    private func getSiteInfoDataSource() -> URLRequestDataSource {
        return try! sut.requestDataSource(for: .getSiteInfo,
                                          bodyInfo: .siteID(FakeParams.fakeSiteID))
    }
    
    private func getRegisterDataSource() -> URLRequestDataSource {
        return try! sut.requestDataSource(for: .register,
                                          bodyInfo: .register(FakeParams.getTestRegisterParams()))
    }
    
    private func getLoginDataSource() -> URLRequestDataSource {
        return try! sut.requestDataSource(for: .login,
                                          bodyInfo: .login(FakeParams.getTestAuthParams()))
    }
    
    private func assertErrorThrownWhenSendingWrongParams(for requestType: AuthRequestType,
                                                         bodyInfo: AuthParamsType,
                                                         line: UInt = #line) {
        do {
            _ = try sut.requestDataSource(for: requestType,
                                          bodyInfo: bodyInfo)
            XCTFail("Should fail because params are not correct", line: line)
        }
        catch AuthRequestDataSourceCreationError.invalidParams {}
        catch {
            XCTFail("Wrong error thrown when params are not correct", line: line)
        }
    }

}
