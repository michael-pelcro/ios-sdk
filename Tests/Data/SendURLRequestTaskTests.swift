//
//  SendURLRequestTaskTests.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import XCTest
@testable import Pelcro

class SendURLRequestTaskTests: XCTestCase {
    
    private var sut: SendURLRequestTask!
    private var urlRequestStub: URLRequest!
    private var urlSessionMock: URLSessionMock!
    
    override func setUp() {
        super.setUp()
        urlRequestStub = URLRequest(url: URL(string: URLRequestDataSourceStub.validURLString)!)
        urlSessionMock = URLSessionMock()
        urlSessionMock.taskToReturn = URLSessionDataTaskSpy(delegate: urlSessionMock)
        sut = SendURLRequestTask(urlSession: urlSessionMock)
    }
    
    override func tearDown() {
        sut = nil
        urlSessionMock = nil
        urlRequestStub = nil
        super.tearDown()
    }

    func test_WhenRunning_AskToCreateATask() {
        sut.run(urlRequest: urlRequestStub, completion: nil)
        XCTAssertEqual(urlSessionMock.requestAskedTaskFor, urlRequestStub,
                       "Ask to create a task with the provided url request.")
    }
    
    func test_WhenRunning_ResumeCreatedTask() {
        sut.run(urlRequest: urlRequestStub, completion: nil)
        XCTAssertTrue(urlSessionMock.taskToReturn!.didResume,
                      "Should resume created data task when running a `send URLRequest task`.")
    }
    
    func test_WhenFinishingTask_CompletionReturnsTaskDataAndError() {
        let expectationToFinishRunning = expectation(description: "To finish running task")
        sut.run(urlRequest: urlRequestStub) {
            XCTAssertEqual(self.urlSessionMock.dataToReturn, $0,
                           "Should complete with data return by the data task.")
            XCTAssertTrue($1 is ErrorStub,
                          "Should complete with error returned by the data task.")
            expectationToFinishRunning.fulfill()
        }
        wait(for: [expectationToFinishRunning], timeout: 0.1)
    }
}
