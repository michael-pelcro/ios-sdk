//
//  KeepInMemoryDataTaskTests.swift
//  PelcroTests
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import XCTest
@testable import Pelcro

class KeepInMemoryDataTaskTests: XCTestCase {
    
    private var sut: KeepInMemoryDataTask!
    private var taskStub: DictionaryURLRequestAsyncTask!
    private let testKey = DictionaryURLRequestAsyncTask.testDataKey
    private let parentTestKey = DictionaryURLRequestAsyncTask.parentTestKey
    
    override func setUp() {
        super.setUp()
        taskStub = DictionaryURLRequestAsyncTask()
        sut = KeepInMemoryDataTask(parentDataKey: parentTestKey,
                                   dataKey: testKey,
                                   urlRequest: FakeParams.urlRequest,
                                   task: taskStub)
    }
    
    override func tearDown() {
        sut = nil
        taskStub = nil
        super.tearDown()
    }

    func test_WhenMemoryDataIsNotAvailable_RunURLRequestTask() {
        runWithExpectation { result, _ in
            let parentDict = self.taskStub.dataToCompleteWith[self.parentTestKey] as? [String: Any]
            XCTAssertEqual(result, parentDict?[self.testKey] as? String)
        }
    }
    
    func test_WhenMemoryDataIsNotAvailable_RunURLRequestTaskWithProvidedURLRequest() {
        sut.runWith(completion: nil)
        XCTAssertEqual(taskStub.receivedURLRequest, FakeParams.urlRequest)
    }
    
    func test_WhenTaskReturnsError_CompleteWithThatError() {
        taskStub.errorToCompleteWith = ErrorStub()
        runWithExpectation { _, error in
            XCTAssertTrue(error is ErrorStub)
        }
    }
    
    func test_WhenRunningTheSecondTime_DontRunTaskButStillCompleteWithCorrectInfo() {
        sut.runWith(completion: nil)
        taskStub.didRun = false
        runWithExpectation { result, _ in
            XCTAssertFalse(self.taskStub.didRun)
            let parentDict = self.taskStub.dataToCompleteWith[self.parentTestKey] as? [String: Any]
            XCTAssertEqual(result, parentDict?[self.testKey] as? String)
        }
    }
    
    //MARK: Helpers
    private func runWithExpectation(completion: ((String?, Error?) -> Void)?) {
        let expectationToFinishTask = expectation(description: "To finish task")
        sut.runWith { result, error in
            completion?(result, error)
            expectationToFinishTask.fulfill()
        }
        wait(for: [expectationToFinishTask], timeout: 0.1)
    }
}
