//
//  URLSessionMock.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation



class URLSessionMock: URLSession, FakeDataTaskDelegate {
    var requestAskedTaskFor: URLRequest?
    var taskToReturn: URLSessionDataTaskSpy?
    var dataToReturn = Data()
    var errorToReturn = ErrorStub()
    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)?
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        requestAskedTaskFor = request
        self.completionHandler = completionHandler
        return taskToReturn!
    }
    
    func didResumeTask() {
        completionHandler?(dataToReturn, nil, errorToReturn)
    }
}
