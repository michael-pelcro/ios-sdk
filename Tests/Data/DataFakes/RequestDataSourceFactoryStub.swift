//
//  RequestDataSourceFactoryStub.swift
//  PelcroTests
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

class RequestDataSourceFactoryStub: RequestDataSourceFactory {
    typealias RequestType = AuthRequestType
    typealias BodyInfoType = AuthParamsType
    
    var receivedRequestType: RequestType?
    var receivedBodyInfo: BodyInfoType?
    var dataSourceToReturn = URLRequestDataSourceStub(httpBody: Data())
    
    func requestDataSource(for requestType: AuthRequestType,
                           bodyInfo: AuthParamsType) throws -> URLRequestDataSource {
        receivedRequestType = requestType
        receivedBodyInfo = bodyInfo
        return dataSourceToReturn
    }
}
