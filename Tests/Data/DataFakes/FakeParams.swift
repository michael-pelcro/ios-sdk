//
//  FakeParams.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

struct FakeParams {
    static let fakeSiteID = "Fake site ID"
    static let fakeAccountID = "Fake account ID"
    static let fakeEmail = "Fake email"
    static let fakePassword = "Fake password"
    static let fakeLanguage = "Fake language"
    
    static let urlRequest = URLRequest(url: URL(string: "https://www.pelcro.com")!)
    
    static func getTestAuthParams() -> AuthParams {
        return AuthParams(siteID: fakeSiteID,
                          email: fakeEmail,
                          password: fakePassword)
    }
    
    static func getTestRegisterParams() -> RegisterParams {
        return RegisterParams(authParams: getTestAuthParams(),
                              accountID: fakeAccountID,
                              language: fakeLanguage)
    }
}
