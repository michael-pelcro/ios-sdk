//
//  DataAsyncTask.swift
//  PelcroTests
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

class DictionaryURLRequestAsyncTask: URLRequestAsyncTask {
    typealias ResultType = [String: Any]
    
    static let testDataKey = "testKey"
    static let parentTestKey = "dataKeyDict"
    
    var dataToCompleteWith: [String: Any] = [DictionaryURLRequestAsyncTask.testDataKey: "value1",
                                             "dataKey2": 12,
                                             DictionaryURLRequestAsyncTask.parentTestKey: [DictionaryURLRequestAsyncTask.testDataKey: "level1Value"]]
    var errorToCompleteWith: Error?
    var receivedURLRequest: URLRequest?
    var didRun = false
    
    func run(urlRequest: URLRequest,
             completion: (([String: Any]?, Error?) -> Void)?) {
        receivedURLRequest = urlRequest
        didRun = true
        completion?(dataToCompleteWith, errorToCompleteWith)
    }
}
