//
//  RequestFactoryStub.swift
//  PelcroTests
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

class RequestFactoryStub: RequestFactory {
    var receivedDataSource: URLRequestDataSource?
    let urlRequestToReturn = FakeParams.urlRequest
    func requestWith(dataSource: URLRequestDataSource) throws -> URLRequest {
        receivedDataSource = dataSource
        return urlRequestToReturn
    }
}
