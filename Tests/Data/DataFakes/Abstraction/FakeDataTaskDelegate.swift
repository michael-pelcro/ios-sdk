//
//  FakeDataTaskDelegate.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

protocol FakeDataTaskDelegate: class {
    func didResumeTask()
}
