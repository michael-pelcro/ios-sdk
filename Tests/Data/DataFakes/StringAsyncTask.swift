//
//  StringAsyncTask.swift
//  PelcroTests
//
//  Created by Corneliu on 06/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

class StringAsyncTask: AsyncTask {
    typealias ResultType = String
    
    var stringToReturn = "test async string result"
    var errorToReturn: Error?
    var receivedCompletion: ((String?, Error?) -> Void)?
    
    func runWith(completion: ((String?, Error?) -> Void)?) {
        completion?(stringToReturn, errorToReturn)
    }
}
