//
//  DataSourceStub.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation
@testable import Pelcro

struct URLRequestDataSourceStub: URLRequestDataSource {
    static let fakeAcceptHeaderFieldValue = "my fake header field accept value"
    static let fakeContentTypeHeaderFieldValue = "my fake header field content type value"
    static let invalidURLString = "fake url"
    static let validURLString = "https://www.pelcro.com"
    var urlString: String
    var httpMethod: HTTPMethod
    var headerValues: [HeaderField: String]
    var httpBody: Data?
    
    init(urlString: String = validURLString,
         httpMethod: HTTPMethod = .get,
         headerValues: [HeaderField: String] = [:],
         httpBody: Data? = nil) {
        self.urlString = urlString
        self.httpMethod = httpMethod
        self.headerValues = headerValues
        self.httpBody = httpBody
    }
}
