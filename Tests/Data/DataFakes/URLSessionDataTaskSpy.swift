//
//  URLSessionDataTaskSpy.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import Foundation

class URLSessionDataTaskSpy: URLSessionDataTask {
    private weak var delegate: FakeDataTaskDelegate?
    init(delegate: FakeDataTaskDelegate) {
        self.delegate = delegate
    }
    var didResume = false
    
    override func resume() {
        didResume = true
        delegate?.didResumeTask()
    }
}
