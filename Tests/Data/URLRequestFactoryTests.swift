//
//  URLRequestFactoryTests.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import XCTest
@testable import Pelcro

class URLRequestFactoryTests: XCTestCase {
    
    private var sut: URLRequestFactory!
    private var dataSourceStub: URLRequestDataSourceStub!
    
    override func setUp() {
        super.setUp()
        sut = URLRequestFactory()
        dataSourceStub = URLRequestDataSourceStub(urlString: URLRequestDataSourceStub.validURLString)
    }
    
    override func tearDown() {
        sut = nil
        dataSourceStub = nil
        super.tearDown()
    }

    func test_WhenProvidingInvalidURL_ThrowInvalidURLError() {
        dataSourceStub.urlString = URLRequestDataSourceStub.invalidURLString
        do {
            _ = try sut.requestWith(dataSource: dataSourceStub)
            XCTFail("Should throw error when providing invalid url")
        }
        catch URLRequestCreationError.invalidURL {}
        catch {
            XCTFail("Should throw correct error when providing invalid url")
        }
    }
    
    func test_WhenProvidingValidURL_ReturnURLRequest() {
        XCTAssertEqual(getRequest().url?.absoluteString, dataSourceStub.urlString)
    }
    
    func test_WhenProvidingGETHTTPMethod_RequestShouldBeConfiguredCorrectly() {
        XCTAssertEqual(getRequest().httpMethod, "GET")
    }
    
    func test_WhenProvidingPOSTHTTPMethod_RequestShouldBeConfiguredCorrectly() {
        dataSourceStub.httpMethod = .post
        XCTAssertEqual(getRequest().httpMethod, "POST")
    }
    
    func test_WhenProvidingHTTPAcceptHeaderValue_RequestShouldIncludeIt() {
        dataSourceStub.headerValues = [.accept: URLRequestDataSourceStub.fakeAcceptHeaderFieldValue]
        let actualValue = getRequest().value(forHTTPHeaderField: "Accept")
        let expectedValue = URLRequestDataSourceStub.fakeAcceptHeaderFieldValue
        XCTAssertEqual(actualValue, expectedValue, "Configure Accept header value correctly")
    }
    
    func test_WhenProvidingHTTPContentTypeHeaderValue_RequestShouldIncludeIt() {
        dataSourceStub.headerValues = [.contentType: URLRequestDataSourceStub.fakeContentTypeHeaderFieldValue]
        let actualValue = getRequest().value(forHTTPHeaderField: "Content-Type")
        let expectedValue = URLRequestDataSourceStub.fakeContentTypeHeaderFieldValue
        XCTAssertEqual(actualValue, expectedValue, "Configure Content-Type header value correctly")
    }
    
    func test_WhenProvidingNoHeaderValues_RequestShouldNotIncludeAny() {
        let numberOfHeaderFieldsDefined = getRequest().allHTTPHeaderFields?.count
        XCTAssertEqual(numberOfHeaderFieldsDefined, 0, "When no header fields are defined, it shouldn't include any")
    }
    
    func test_WhenProvidingNoHTTPBody_RequestShouldNotIncludeAny() {
        XCTAssertNil(getRequest().httpBody, "When no body is defined, it shouldn't include any")
    }
    
    func test_WhenProvidingHTTPBody_RequestShouldIncludeIt() {
        let expectedBody = Data()
        dataSourceStub.httpBody = expectedBody
        XCTAssertEqual(getRequest().httpBody, expectedBody, "When no body is defined, it shouldn't include any")
    }
    
    //MARK: Helper functions
    private func getRequest() -> URLRequest {
        return try! sut.requestWith(dataSource: dataSourceStub)
    }
}
