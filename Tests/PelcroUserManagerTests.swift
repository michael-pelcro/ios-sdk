//
//  PelcroUserManagerTests.swift
//  PelcroTests
//
//  Created by Corneliu on 05/05/2019.
//  Copyright © 2019 Pelcro. All rights reserved.
//

import XCTest
@testable import Pelcro

class PelcroUserManagerTests: XCTestCase {
    
    private let testSiteID = "test site id"
    
    private var sut: PelcroUserManager!
    private var dataSourceFactoryStub: RequestDataSourceFactoryStub!
    private var accountIDDataProviderStub: StringAsyncTask!
    private var requestFactoryStub: RequestFactoryStub!
    private var sendRequestTask: DictionaryURLRequestAsyncTask!
    
    override func setUp() {
        super.setUp()
        accountIDDataProviderStub = StringAsyncTask()
        dataSourceFactoryStub = RequestDataSourceFactoryStub()
        requestFactoryStub = RequestFactoryStub()
        sendRequestTask = DictionaryURLRequestAsyncTask()
        Pelcro.shared.siteID = testSiteID
        sut = PelcroUserManager(requestDataSourceFactory: dataSourceFactoryStub,
                                  accountIDDataProvider: accountIDDataProviderStub,
                                  requestFactory: requestFactoryStub,
                                  sendRequestTask: sendRequestTask)
    }
    
    override func tearDown() {
        sut = nil
        Pelcro.shared.siteID = nil
        accountIDDataProviderStub = nil
        dataSourceFactoryStub = nil
        requestFactoryStub = nil
        sendRequestTask = nil
        super.tearDown()
    }
    
    //MARK: Register request tests
    func test_WhenSiteIDNotProvided_RegisterParamsIncludeAnEmptySiteID() {
        Pelcro.shared.siteID = nil
        sendTestRegisterRequest()
        XCTAssertEqual(getRegisterParams()?.authParams.siteID, "")
    }
    
    func test_WhenSendingRegisterRequest_AskForRequestDataSourceWithCorrectRequestType() {
        sendTestRegisterRequest()
        XCTAssertEqual(dataSourceFactoryStub.receivedRequestType, .register)
    }
    
    func test_WhenSendingRegisterRequest_AskForRequestDataSourceWithCorrectBodyInfoType() {
        sendTestRegisterRequest()
        XCTAssertNotNil(getRegisterParams())
    }
    
    func test_WhenSendingRegisterRequest_BodyInfoIncludesProvidedAccountID() {
        sendTestRegisterRequest()
        XCTAssertEqual(getRegisterParams()?.accountID, accountIDDataProviderStub.stringToReturn)
    }
    
    func test_WhenSendingRegisterRequest_BodyInfoIncludesProvidedSiteID() {
        sendTestRegisterRequest()
        XCTAssertEqual(getRegisterParams()?.authParams.siteID, testSiteID)
    }
    
    func test_WhenSendingRegisterRequest_BodyInfoIncludesProvidedEmail() {
        sendTestRegisterRequest()
        XCTAssertEqual(getRegisterParams()?.authParams.email, FakeParams.fakeEmail)
    }
    
    func test_WhenSendingRegisterRequest_BodyInfoIncludesProvidedPassword() {
        sendTestRegisterRequest()
        XCTAssertEqual(getRegisterParams()?.authParams.password, FakeParams.fakePassword)
    }
    
    func test_WhenSendingRegisterRequest_BodyInfoIncludesCorrectLanguageParam() {
        sendTestRegisterRequest()
        XCTAssertEqual(getRegisterParams()?.language, PelcroUserManager.language)
    }
    
    func test_WhenSendingRegisterRequest_CreateURLRequestWithProvidedDataSource() {
        sendTestRegisterRequest()
        XCTAssertEqual(dataSourceFactoryStub.dataSourceToReturn.urlString, requestFactoryStub.receivedDataSource?.urlString)
    }
    
    func test_WhenAccountIDProviderRepliesWithError_DontAskForRequestDataSource() {
        accountIDDataProviderStub.errorToReturn = ErrorStub()
        sendTestRegisterRequest()
        XCTAssertNil(dataSourceFactoryStub.receivedRequestType)
    }
    
    func test_WhenAccountIDProviderRepliesWithError_CompleteWithError() {
        accountIDDataProviderStub.errorToReturn = ErrorStub()
        sendExpectedRegisterRequest {
            XCTAssertNil($0.data, "Should retrieve no result when account ID error has occurred")
            XCTAssertTrue($0.error is ErrorStub, "Should retrieve expected error")
        }
    }
    
    func test_WhenSendingRegisterRequest_SendRequestWithCreatedURLRequest() {
        sendExpectedRegisterRequest { _ in
            XCTAssertEqual(self.sendRequestTask.receivedURLRequest, self.requestFactoryStub.urlRequestToReturn, "Should run send request task with correct URL request")
        }
    }
    
    func test_WhenRegisterRequestSenderReturnsError_CompleteWithThatError() {
        sendRequestTask.errorToCompleteWith = ErrorStub()
        sendExpectedRegisterRequest {
            XCTAssertTrue($0.error is ErrorStub, "Should complete with error when request task completes with error")
        }
    }
    
    func test_WhenRegisterRequestSenderReturnsDictionary_CompleteWithThatDictionary() {
        sendExpectedRegisterRequest {
            self.assertCorrectDictionaryIsReturned(result: $0)
        }
    }
    
    //MARK: Login tests
    func test_WhenSiteIDNotProvided_LoginParamsIncludeAnEmptySiteID() {
        Pelcro.shared.siteID = nil
        sendTestLoginRequest()
        XCTAssertEqual(getLoginParams()?.siteID, "")
    }
    
    func test_WhenSendingLoginRequest_AskForRequestDataSourceWithCorrectRequestType() {
        sendTestLoginRequest()
        XCTAssertEqual(dataSourceFactoryStub.receivedRequestType, .login)
    }
    
    func test_WhenSendingLoginRequest_AskForRequestDataSourceWithCorrectBodyInfoType() {
        sendTestLoginRequest()
        XCTAssertNotNil(getLoginParams())
    }
    
    func test_WhenSendingLoginRequest_BodyInfoIncludesProvidedSiteID() {
        sendTestLoginRequest()
        XCTAssertEqual(getLoginParams()?.siteID, testSiteID)
    }
    
    func test_WhenSendingLoginRequest_BodyInfoIncludesProvidedEmail() {
        sendTestLoginRequest()
        XCTAssertEqual(getLoginParams()?.email, FakeParams.fakeEmail)
    }
    
    func test_WhenSendingLoginRequest_BodyInfoIncludesProvidedPassword() {
        sendTestLoginRequest()
        XCTAssertEqual(getLoginParams()?.password, FakeParams.fakePassword)
    }
    
    func test_WhenSendingLoginRequest_CreateURLRequestWithProvidedDataSource() {
        sendTestLoginRequest()
        XCTAssertEqual(dataSourceFactoryStub.dataSourceToReturn.urlString, requestFactoryStub.receivedDataSource?.urlString)
    }
    
    func test_WhenSendingLoginRequest_SendRequestWithCreatedURLRequest() {
        sendExpectedLoginRequest { _ in
            XCTAssertEqual(self.sendRequestTask.receivedURLRequest, self.requestFactoryStub.urlRequestToReturn, "Should run send request task with correct URL request")
        }
    }
    
    func test_WhenLoginRequestSenderReturnsError_CompleteWithThatError() {
        sendRequestTask.errorToCompleteWith = ErrorStub()
        sendExpectedLoginRequest {
            XCTAssertTrue($0.error is ErrorStub, "Should complete with error when request task completes with error")
        }
    }
    
    func test_WhenLoginRequestSenderReturnsDictionary_CompleteWithThatDictionary() {
        sendExpectedLoginRequest {
            self.assertCorrectDictionaryIsReturned(result: $0)
        }
    }
    
    
    //MARK: Helpers
    private func sendTestRegisterRequest(completion: PelcroUserManager.AuthResultCompletion? = nil) {
        sut.register(with: FakeParams.fakeEmail,
                     password: FakeParams.fakePassword) {
                        completion?($0)
        }
    }
    private func sendTestLoginRequest(completion: PelcroUserManager.AuthResultCompletion? = nil) {
        sut.login(with: FakeParams.fakeEmail,
                  password: FakeParams.fakePassword) {
                    completion?($0)
        }
    }
    
    private func getRegisterParams(line: UInt = #line) -> RegisterParams? {
        guard case let .register(bodyInfo)? = dataSourceFactoryStub.receivedBodyInfo else {
            XCTFail("received incorrect params type", line: line)
            return nil
        }
        return bodyInfo
    }
    
    private func getLoginParams(line: UInt = #line) -> AuthParams? {
        guard case let .login(authParams)? = dataSourceFactoryStub.receivedBodyInfo else {
            XCTFail("received incorrect params type", line: line)
            return nil
        }
        return authParams
    }
    
    private func sendExpectedRegisterRequest(completion: PelcroUserManager.AuthResultCompletion? = nil) {
        let expectationToSendCompletion = expectation(description: "To complete request")
        sendTestRegisterRequest {
            completion?($0)
            expectationToSendCompletion.fulfill()
        }
        wait(for: [expectationToSendCompletion], timeout: 0.1)
    }
    
    private func sendExpectedLoginRequest(completion: PelcroUserManager.AuthResultCompletion? = nil) {
        let expectationToSendCompletion = expectation(description: "To complete request")
        sendTestLoginRequest {
            completion?($0)
            expectationToSendCompletion.fulfill()
        }
        wait(for: [expectationToSendCompletion], timeout: 0.1)
    }
    
    private func assertCorrectDictionaryIsReturned(result: PelcroResult?, line: UInt = #line) {
        guard result?.error == nil else {
            XCTFail("Should return no error", line: line)
            return
        }
        guard let result = result?.data else {
            XCTFail("Should return result", line: line)
            return
        }
        let expectedValue = self.sendRequestTask.dataToCompleteWith["dataKey1"] as? String
        let actualValue = result["dataKey1"] as? String
        XCTAssertEqual(actualValue, expectedValue, "Should complete with correct dictionary when request task completes.", line: line)
    }
}
