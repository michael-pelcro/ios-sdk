# 1.0.0

- The Pelcro SDK includes a authentication API usable for register and login.

# 1.1.0

The Pelcro SDK includes:
- authentication/user API usable for register, login, refresh, logout.
- site API usable for get site info.
- subscription API usable for create, cancel, reactivate subscriptions.

# 1.1.1

Added possibility to choose environment (Staging, Prod) for Pelcro.

# 1.1.2

Update Prod environment url

# 1.1.3

Added handling of new SKError cases. 

# 1.1.4

Added handling of new SKError introduced in iOS 14.5. 
